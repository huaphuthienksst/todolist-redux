import React from "react";

export default function DemoChildren(props) {
  return (
    <div>
      <h3>*DemoChildren</h3>
      {props.children}
    </div>
  );
}
