import { combineReducers } from "redux";
import userReducer from "./user";
import toToListReducer from "./todolist-reducer";
const rootReducer = combineReducers({
  //key: value
  userReducer, //   userReducer: userReducer,
  toToListReducer,
});

export default rootReducer;
