let initialState = {
  completedList: [],
  uncompletedList: [],
};

const toToListReducer = (state = initialState, action) => {
  //     console.log(action);
  switch (action.type) {
    case "ADD_TASK":
      const newContent = { ...action.payload, id: Math.random() };
      const newList = [...state.uncompletedList, newContent];
      state.uncompletedList = newList;
      return { ...state };
    case "DELETE_UNCOMPLETED_TASK":
      const index = state.uncompletedList.findIndex((item) => {
        return item.id === action.payload.id;
      });
      let uncompletedList = [...state.uncompletedList];
      if (index !== -1) {
        uncompletedList.splice(index, 1);
        state.uncompletedList = uncompletedList;
      }
      return { ...state };
    case "DELETE_COMPLETED_TASK":
      {
        const index = state.completedList.findIndex((item) => {
          return item.id === action.payload.id;
        });
        let completedList = [...state.completedList];
        if (index !== -1) {
          completedList.splice(index, 1);
          state.completedList = completedList;
        }
      }
      return { ...state };
    case "CHANGE_TO_COMPLETED_LIST":
      {
        const newCompltedList = [...state.completedList, action.payload];
        state.completedList = newCompltedList;
        const index = state.uncompletedList.findIndex((item) => {
          return item.id === action.payload.id;
        });
        let uncompletedList = [...state.uncompletedList];
        if (index !== -1) {
          uncompletedList.splice(index, 1);
          state.uncompletedList = uncompletedList;
        }
      }
      return { ...state };
    case "CHANGE_TO_UNCOMPLETED_LIST":
      {
        const newUncompltedList = [...state.uncompletedList, action.payload];
        state.uncompletedList = newUncompltedList;
        const index = state.completedList.findIndex((item) => {
          return item.id === action.payload.id;
        });
        let completedList = [...state.completedList];
        if (index !== -1) {
          completedList.splice(index, 1);
          state.completedList = completedList;
        }
      }
      return { ...state };
    default:
      return { ...state };
  }
};

export default toToListReducer;
