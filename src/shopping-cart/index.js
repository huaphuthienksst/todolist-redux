import React, { Component } from "react";
import DanhSachSanPham from "./danh-sach-san-pham";
import Modal from "./modal";
import data from "./data.json";

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: data,
      detailProduct: data[0],
      listCart: [],
    };
  }

  handleGetDetailProduct = (product) => {
    this.setState({
      detailProduct: product,
    });
  };

  _findIndex = (maSP) => {
    return this.state.listCart.findIndex((item) => {
      return item.maSP === maSP;
    });
  };

  /**
   * Thêm product trong mảng listCart
   */
  handleGetProductAddCart = (product) => {
    /**
     * Thêm product vào state.listCart
     */
    let productNew = {
      maSP: product.maSP,
      tenSP: product.tenSP,
      hinhAnh: product.hinhAnh,
      soLuong: 1,
      giaBan: product.giaBan,
    };

    let listCart = [...this.state.listCart];

    //Tìm vị trí
    let viTri = this._findIndex(productNew.maSP);

    if (viTri !== -1) {
      //Nếu sp tồn tại trong listCart thì cập nhập SL
      listCart[viTri].soLuong += 1;
    } else {
      //Thêm sp vào listCart
      listCart = [...this.state.listCart, productNew];
    }

    this.setState({
      listCart: listCart,
    });
  };

  /**
   * Xóa product trong mảng listCart
   */
  handleDeleteProduct = (product) => {
    let viTri = this._findIndex(product.maSP);
    let listCart = [...this.state.listCart];
    if (viTri !== -1) {
      //Xóa
      listCart.splice(viTri, 1);

      this.setState({
        listCart,
      });
    }
  };

  handleUpdateSL = (product, status) => {
    /**
     * 0. Copy lại mảng this.state.listCart
     * 1. Tìm vị trí
     * 2. So sánh status là true => tăng
     *    Ngược lại: => Giảm
     * 3. setState
     */
    let listCart = [...this.state.listCart];
    let viTri = this._findIndex(product.maSP);
    if (viTri !== -1) {
      if (status) {
        //Tăng SL
        listCart[viTri].soLuong += 1;
      } else {
        //Giảm SL
        if (listCart[viTri].soLuong > 1) {
          listCart[viTri].soLuong -= 1;
        }
      }

      this.setState({
        listCart,
      });
    }
  };

  handleShowSL = () => {
    let listCart = [...this.state.listCart];

    let tong = 0;
    listCart.forEach((item) => {
      tong += item.soLuong;
    });

    return tong;
  };

  render() {
    const { detailProduct, listCart } = this.state;
    return (
      <div>
        <h3 className="title">Bài tập giỏ hàng</h3>
        <div className="container">
          <button
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng ({this.handleShowSL()})
          </button>
        </div>
        <DanhSachSanPham
          listProduct={this.state.listProduct}
          getDetailProduct={this.handleGetDetailProduct}
          getProductAddCart={this.handleGetProductAddCart}
        />
        <Modal
          listCart={listCart}
          getProductDelete={this.handleDeleteProduct}
          getProductUpdateSL={this.handleUpdateSL}
        />
        <div className="container">
          <div className="row">
            <div className="col-sm-5">
              <img className="img-fluid" src={detailProduct.hinhAnh} alt="" />
            </div>
            <div className="col-sm-7">
              <h3>Thông số kỹ thuật</h3>
              <table className="table">
                <tbody>
                  <tr>
                    <td>Màn hình</td>
                    <td>{detailProduct.manHinh}</td>
                  </tr>
                  <tr>
                    <td>Hệ điều hành</td>
                    <td>{detailProduct.heDieuHanh}</td>
                  </tr>
                  <tr>
                    <td>Camera trước</td>
                    <td>{detailProduct.cameraTruoc}</td>
                  </tr>
                  <tr>
                    <td>Camera sau</td>
                    <td>{detailProduct.cameraSau}</td>
                  </tr>
                  <tr>
                    <td>RAM</td>
                    <td>{detailProduct.ram}</td>
                  </tr>
                  <tr>
                    <td>ROM</td>
                    <td>{detailProduct.rom}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
