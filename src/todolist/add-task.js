import React, { Component } from "react";
import { connect } from "react-redux";
class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      id: "",
      type: "uncompleted",
    };
  }
  handleOnChange = (event) => {
    console.log(event.target.value);
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };
  handleAddTask = () => {
    this.props.addTask(this.state);
    this.setState({
      content: "",
    });
  };
  render() {
    return (
      <>
        <input
          name="content"
          id="newTask"
          type="text"
          placeholder="Enter an activity..."
          onChange={this.handleOnChange}
          value={this.state.content}
        />
        <button id="addItem" onClick={this.handleAddTask}>
          <i className="fa fa-plus" />
        </button>
      </>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addTask: (data) => {
      const action = {
        type: "ADD_TASK",
        payload: data,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(AddTask);
