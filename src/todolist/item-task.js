import React, { Component } from "react";
import { connect } from "react-redux";
class ItemTask extends Component {
  handleDelete = () => {
    if (this.props.task.type === "uncompleted") {
      this.props.deleteUncompletedTask(this.props.task);
    } else {
      this.props.deleteCompletedTask(this.props.task);
    }
  };
  handleChange = () => {
    let { task } = this.props;
    if (task.type === "uncompleted") {
      task.type = "completed";
      this.props.changeToCompletedList(task);
    } else {
      task.type = "uncompleted";
      this.props.changeToUncompletedList(task);
    }
  };
  render() {
    const { task } = this.props;
    console.log(task);
    return (
      <li>
        <span className="liName">{task.content}</span>
        <span>
          <i className="fas fa-trash-alt" onClick={this.handleDelete} />
          <i className="far fa-check-circle" onClick={this.handleChange} />
        </span>
      </li>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    deleteUncompletedTask: (task) => {
      const action = {
        type: "DELETE_UNCOMPLETED_TASK",
        payload: task,
      };
      dispatch(action);
    },
    deleteCompletedTask: (task) => {
      const action = {
        type: "DELETE_COMPLETED_TASK",
        payload: task,
      };
      dispatch(action);
    },
    changeToCompletedList: (task) => {
      const action = {
        type: "CHANGE_TO_COMPLETED_LIST",
        payload: task,
      };
      dispatch(action);
    },
    changeToUncompletedList: (task) => {
      const action = {
        type: "CHANGE_TO_UNCOMPLETED_LIST",
        payload: task,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemTask);
