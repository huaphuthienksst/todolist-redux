import React, { Component } from "react";
import ItemTask from "./item-task";
import { connect } from "react-redux";
class ListTask extends Component {
  renderUncompletedList = () => {
    return this.props.listUncompletedTask.map((item) => {
      return <ItemTask key={item.id} task={item} />;
    });
  };
  renderCompletedList = () => {
    return this.props.listCompletedTask.map((item) => {
      return <ItemTask key={item.id} task={item} />;
    });
  };
  render() {
    console.log(this.props.listUncompletedTask);
    return (
      <>
        <ul className="todo" id="todo">
          {this.renderUncompletedList()}
        </ul>
        {/* Completed tasks */}
        <ul className="todo" id="completed">
          {this.renderCompletedList()}
        </ul>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listUncompletedTask: state.toToListReducer.uncompletedList,
    listCompletedTask: state.toToListReducer.completedList,
  };
};

export default connect(mapStateToProps, null)(ListTask);
